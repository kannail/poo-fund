# POO fund

POO : Class Diagram and Kotlin Code

## 1. Coder le point d’entrée du système :

Il n'y a pas besoin de point d'entree en python

## 2. Ajouter la classe du cheval avec un constructeur primaire :

class Horse:
    def __init__(self, name):
        self.name = name

## 3. Créez une instance de cheval et affichez la :

horse = Horse("Beauty")
print(horse.name)

## 4. Exécutez. Qu’affiche la console ?

PS C:\Users\aslan\Documents\share\Cours\python\POO\poo-fund> python .\horse.py
Beauty

## 5. Créez une deuxième instance, affichez la et exécutez.

horse = Horse("Beauty")
horse2 = Horse("Black Beauty")
print(horse.name)
print(horse2.name)

## 6. Ajouter la fonctiontoString()à la classeHorse:

from abc import ABC, abstractmethod

class Horse:
    def __init__(self, name):
        self.name = name

    @abstractmethod
    def toString(self):
        return

## 7. Exécutez. Qu’affiche la console ?

PS C:\Users\aslan\Documents\share\Cours\python\POO\poo-fund> python .\horse.py
Beauty
Black Beauty

## 8. Ajouter la fonctionrun()à la classeHorse:

from abc import ABC, abstractmethod

class Horse:
    def __init__(self, name):
        self.name = name

    def run(self):
        r = "running"
        return r

    @abstractmethod
    def toString(self):
        return

## 9. Testez la fonction run()dans le main(), exécutez. Qu’affiche la console ?

running

## 10. Ajoutez une fonction neigh(), testez la dans la fonction main(), exé-cutez. Qu’affiche la console ?

from abc import ABC, abstractmethod

class Horse:
    def __init__(self, name):
        self.name = name

    def run(self):
        r = "running"
        return r

    def neigh(self):
        n = "neighing"
        return n

    @abstractmethod
    def toString(self):
        return

neighing
