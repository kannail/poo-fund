from abc import ABC, abstractmethod

class Horse:
    def __init__(self, name):
        self.name = name

    def run(self):
        r = "running"
        return r

    def neigh(self):
        n = "neighing"
        return n

    @abstractmethod
    def toString(self):
        return

class Unicorn(Horse):
    def __init__(self, name):
        super().__init__(name)
        self.name = name

    def dance(self):
        d = "dancing"
        return d

    def sing(self):
        s = "singing"
        return s

    def fly(self):
        f = "flying"
        return f

    def toString(self):
        u = "-*-==//////((''"
        return u

horse = Horse("Spirit")
horse2 = Unicorn("123")
print(horse2.toString())
print(horse.name)